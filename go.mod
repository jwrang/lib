module gitlab.com/jwrang/lib

go 1.22

require (
	github.com/rkoesters/xdg v0.0.1
	golang.org/x/image v0.15.0
	golang.org/x/net v0.21.0
)
