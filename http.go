package lib

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
)

func Download(url, path string) error {
	if Exists(path) {
		return nil
	}
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("%q bad status: %v", url, resp.Status)
	}

	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	dir := filepath.Dir(path)
	err = os.MkdirAll(dir, Dirperm)
	Check(err)
	return os.WriteFile(path, body, Fileperm)
}

func GetJson(url string, res any) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		return fmt.Errorf("%q bad status: %v", url, resp.Status)
	}

	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(&res)
	if err != nil {
		return err
	}
	return nil
}

func ServeDir(dir string) {
	path := fmt.Sprintf("/%v/", dir)
	http.Handle("GET "+path, http.StripPrefix(path, http.FileServer(http.Dir(dir))))
}

func HttpErr(w http.ResponseWriter, err error) {
	Log(err)
	http.Error(w, err.Error(), http.StatusInternalServerError)
}

func StartServer(port int) {
	ports := fmt.Sprintf(":%v", port)
	fmt.Println("Running on http://" + IP + ports)
	Check(http.ListenAndServe(ports, nil))
}
