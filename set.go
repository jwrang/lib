package lib

import (
	"bufio"
	"fmt"
	"os"
)

type Set struct {
	Path string
	Map  map[string]struct{}
}

func LoadSet(path string) *Set {
	f, err := os.Open(path)
	if err != nil {
		fmt.Println("Missing set", path)
		return &Set{Path: path, Map: map[string]struct{}{}}
	}
	defer f.Close()
	scan := bufio.NewScanner(f)
	m := map[string]struct{}{}
	for scan.Scan() {
		line := scan.Text()
		if line != "" {
			m[line] = struct{}{}
		}
	}
	return &Set{Path: path, Map: m}
}

func (s *Set) Add(line string) {
	if s.Has(line) {
		return
	}

	AppendFile(s.Path, line)
	s.Map[line] = struct{}{}
}

func (s *Set) Has(line string) bool {
	_, ok := s.Map[line]
	return ok
}

func (s *Set) Len() int {
	return len(s.Map)
}

func LoadList(path string) []string {
	var s []string
	f, err := os.Open(path)
	if err != nil {
		fmt.Println("Missing file", path)
		return s
	}
	defer f.Close()
	scan := bufio.NewScanner(f)
	for scan.Scan() {
		line := scan.Text()
		if line != "" {
			s = append(s, line)
		}
	}

	return s
}
