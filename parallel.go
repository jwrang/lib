package lib

import "sync"

func Parallel[T any](elements []T, callback func(el T) error) error {
	var wg sync.WaitGroup
	wg.Add(len(elements))

	errs := map[int]error{}

	for i := range elements {
		i := i

		go func() {
			err := callback(elements[i])
			if err != nil {
				errs[i] = err
			}

			wg.Done()
		}()
	}

	wg.Wait()

	for _, err := range errs {
		return err
	}

	return nil
}

func ParallelMap[T any, R any](elements []T, mapper func(el T) (R, error)) ([]R, error) {
	var wg sync.WaitGroup
	wg.Add(len(elements))
	res := make([]R, len(elements))
	errs := map[int]error{}

	for i := range elements {
		i := i

		go func() {
			r, err := mapper(elements[i])
			if err != nil {
				errs[i] = err
			}
			res[i] = r
			wg.Done()
		}()
	}

	wg.Wait()

	for _, err := range errs {
		return nil, err
	}

	return res, nil
}
