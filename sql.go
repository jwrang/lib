package lib

import "regexp"

type SQL struct {
	Tables  []string
	queries map[string]string
}

func (q *SQL) Query(name string) string {
	return q.queries[name]
}

var sqlRegex = regexp.MustCompile(`-- (\w*)\s*(\w[\w\s\(\),$\[\]:]*;)`)

func ParseSql(sql string) *SQL {
	queries := map[string]string{}
	var tables []string

	ms := sqlRegex.FindAllStringSubmatch(sql, -1)
	for _, m := range ms {
		name := m[1]
		query := m[2]
		if name == "tbl" {
			tables = append(tables, query)
		} else {
			queries[name] = query
		}
	}
	return &SQL{
		Tables:  tables,
		queries: queries,
	}
}
