package lib

import (
	"os"
	"strings"

	"golang.org/x/net/html"
)

func ParseHtml(path string) *html.Node {
	f, err := os.Open(path)
	Check(err)
	defer f.Close()
	root, err := html.Parse(f)
	Check(err)
	return root
}

func IterChildren(root *html.Node, cb func(*html.Node)) {
	for child := root.FirstChild; child != nil; child = child.NextSibling {
		cb(child)
	}
}

func FindById(root *html.Node, id string) *html.Node {
	if hasId(root, id) {
		return root
	}

	for child := root.FirstChild; child != nil; child = child.NextSibling {
		if found := FindById(child, id); found != nil {
			return found
		}
	}
	return nil
}

func NodeText(root *html.Node) string {
	nodes := findAll(root, func(n *html.Node) bool { return n.Type == html.TextNode })
	strs := make([]string, 0, len(nodes))
	for _, n := range nodes {
		str := strings.TrimSpace(n.Data)
		if str != "" {
			strs = append(strs, str)
		}
	}
	return strings.Join(strs, " ")
}

func Attr(root *html.Node, attr string) string {
	for _, a := range root.Attr {
		if a.Key == attr {
			return a.Val
		}
	}
	return ""
}

func hasId(root *html.Node, id string) bool {
	for _, attr := range root.Attr {
		if attr.Key == "id" && attr.Val == id {
			return true
		}
	}
	return false
}

func findAll(root *html.Node, matcher func(*html.Node) bool) []*html.Node {
	var matches []*html.Node

	if matcher(root) {
		matches = append(matches, root)
		return matches
	}

	for child := root.FirstChild; child != nil; child = child.NextSibling {
		if cmatches := findAll(child, matcher); cmatches != nil {
			matches = append(matches, cmatches...)
		}
	}

	return matches
}
