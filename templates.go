package lib

import (
	"html/template"
	"net/http"
	"path/filepath"
	"time"
)

const (
	dateTime = "2006-01-02 15:04:05"
)

var funcs = template.FuncMap{
	"add": func(a, b int) int {
		return a + b
	},
	"sub": func(a, b int) int {
		return a - b
	},
	"trunc": func(str string, length int) string {
		if len(str) > length {
			return str[:length] + "..."
		}
		return str
	},
	"fmtdate": func(tim time.Time) string {
		return tim.Format(dateTime)
	},
	"fmtnum": func(i int) string {
		return FmtNum(i)
	},
}

func RenderTemplate(w http.ResponseWriter, temp string, data any) {
	basePath := filepath.Join(TemplDir, "base.html")
	tempPath := filepath.Join(TemplDir, temp)

	t, err := template.New("").Funcs(funcs).ParseFiles(tempPath, basePath)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = t.ExecuteTemplate(w, "base", data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
