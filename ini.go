package lib

import (
	"bufio"
	"os"
	"reflect"
	"regexp"
	"strings"
)

var (
	secReg     = regexp.MustCompile(`^\[([\w\s]*)\]$`)
	valReg     = regexp.MustCompile(`^(\w+)=(.*)$`)
	commentReg = regexp.MustCompile(`^\s*;`)
	empty      = regexp.MustCompile(`^\s*$`)
)

func ParseIni[T any](path string) ([]T, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	scan := bufio.NewScanner(f)

	var sections []T

	var typeT T
	fields := map[string]int{}
	typ := reflect.TypeOf(typeT)
	for i := 0; i < typ.NumField(); i++ {
		fields[strings.ToLower(typ.Field(i).Name)] = i
	}

	var sec T
	first := true
	one := true

	for scan.Scan() {
		line := scan.Text()

		if empty.MatchString(line) || commentReg.MatchString(line) {
			continue
		} else if secReg.MatchString(line) {
			if !first {
				sections = append(sections, sec)
				var zero T
				sec = zero
				one = false
			}

			first = false
			secname := secReg.FindStringSubmatch(line)[1]

			secref := reflect.ValueOf(&sec).Elem()
			if idx, ok := fields["name"]; ok {
				secref.Field(idx).SetString(secname)
			}
		} else if valReg.MatchString(line) {
			submatch := valReg.FindStringSubmatch(line)
			key, val := submatch[1], submatch[2]

			secref := reflect.ValueOf(&sec).Elem()

			if idx, ok := fields[key]; ok {
				secref.Field(idx).SetString(val)
			}
		}
	}

	if !first || one {
		sections = append(sections, sec)
	}
	return sections, nil
}

func AppendIni[T any](file string, section T) {
	var sb strings.Builder
	secval := reflect.ValueOf(&section).Elem()
	name := secval.FieldByName("Name")
	typ := secval.Type()

	sb.WriteString("\n[")
	sb.WriteString(strings.ToLower(strings.TrimSpace(name.String())))
	sb.WriteString("]\n")

	for i := range typ.NumField() {
		f := typ.Field(i)
		key := strings.ToLower(strings.TrimSpace(f.Name))
		val := strings.ToLower(strings.TrimSpace(secval.Field(i).String()))

		if key == "name" {
			continue
		}

		sb.WriteString(key)
		sb.WriteString("=")
		sb.WriteString(val)
		sb.WriteString("\n")
	}

	f, err := os.OpenFile(file, os.O_CREATE|os.O_APPEND|os.O_WRONLY, Fileperm)
	Check(err)
	defer f.Close()
	_, err = f.WriteString(sb.String())
	Check(err)
}
