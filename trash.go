package lib

import (
	"github.com/rkoesters/xdg/trash"
)

func TrashFile(path string) {
	err := trash.Trash(path)
	if err != nil {
		Log(err)
	}
}
