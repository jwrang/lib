package lib

import (
	"os/exec"
)

func GitUpdate() {
	Check(exec.Command("git", "add", ".").Run())
	Check(exec.Command("git", "commit", "-m", `"update"`).Run())
}
