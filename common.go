package lib

import (
	"fmt"
	"net"
	"os/exec"
	"path/filepath"
	"runtime"
	"strconv"
	"syscall"
)

const (
	Fileperm = 0644
	Dirperm  = 0755
)

var (
	IP = getIP()

	root      = filepath.Join(".")
	DataDir   = filepath.Join(root, "data")
	ImgDir    = filepath.Join(root, "images")
	TnDir     = filepath.Join(root, ".thumbnails")
	StaticDir = filepath.Join(root, "static")
	TemplDir  = filepath.Join(root, "templates")

	errFile = filepath.Join(DataDir, "errors")
)

func Mod(i, length int) int {
	return (i%length + length) % length
}

func ByteSize(bytes int) string {
	const (
		gb = 1e9
		mb = 1e6
		kb = 1e3
	)

	if bytes > gb {
		return fmt.Sprintf("%.1f GB", float64(bytes)/gb)
	} else if bytes > mb {
		return fmt.Sprintf("%.1f MB", float64(bytes)/mb)
	}
	return fmt.Sprintf("%.1f KB", float64(bytes)/kb)

}

func FmtNum(i int) string {
	s := strconv.Itoa(i)
	if len(s) > 3 {
		bp := len(s) - 3
		return s[:bp] + " " + s[bp:]
	}
	return s
}

func Reverse[S ~[]E, E any](s S) {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
}

func getIP() string {
	addrs, err := net.InterfaceAddrs()
	Check(err)
	for _, address := range addrs {
		// check the address type and if it is not a loopback the display it
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	panic("no ip")
}

func OpenUrl(url string) {
	var err error

	switch runtime.GOOS {
	case "linux":
		cmd := exec.Command("xdg-open", url)
		cmd.SysProcAttr = &syscall.SysProcAttr{
			Setpgid: true,
		}
		err = cmd.Start()
	case "windows":
		err = exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		err = exec.Command("open", url).Start()
	default:
		err = fmt.Errorf("unsupported platform")
	}
	if err != nil {
		fmt.Printf("could not open %v: %v\n", url, err)
	}
}
