package lib

import (
	"fmt"
	"log"
	"os"
	"time"
)

const (
	timeLayout = "2006/01/02 15:04:05"
)

func Check(err error) {
	if err != nil {
		panic(err)
	}
}

func Log(err error) {
	fmt.Println(err)

	f, openErr := os.OpenFile(errFile, os.O_CREATE|os.O_APPEND|os.O_WRONLY, Fileperm)
	if openErr != nil {
		fmt.Println(openErr)
		return
	}
	defer f.Close()

	l := log.New(f, "", log.LstdFlags)
	l.Println(err)
}

func ClearErrors() {
	err := os.WriteFile(errFile, nil, Fileperm)
	Check(err)
}

type Error struct {
	Time time.Time
	Err  string
}

func GetErrors() []Error {
	errorStrs := LoadList(errFile)
	errors := make([]Error, 0, len(errorStrs))

	for _, errStr := range errorStrs {
		timePart := errStr[:len(timeLayout)]
		errPart := errStr[len(timeLayout)+1:]
		tim, err := time.Parse(timeLayout, timePart)
		Check(err)
		errors = append(errors, Error{
			Time: tim,
			Err:  errPart,
		})
	}
	return errors
}
