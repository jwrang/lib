package lib

import (
	"os"
	"path/filepath"
)

func ReplaceFile(src string, data []byte) {
	copy := src + ".copy"
	err := os.WriteFile(copy, data, Fileperm)
	Check(err)
	err = os.Rename(copy, src)
	Check(err)
}

func AppendFile(name string, line string) {
	f, err := os.OpenFile(name, os.O_CREATE|os.O_APPEND|os.O_WRONLY, Fileperm)
	Check(err)
	defer f.Close()
	_, err = f.WriteString(line + "\n")
	Check(err)
}

func Exists(path string) bool {
	_, err := os.Stat(path)
	return err == nil
}

func MoveToFolder(file, folder string) error {
	dest := filepath.Join(folder, filepath.Base(file))
	return os.Rename(file, dest)
}

// func MergeFolder(from, to string) error {
// 	dir, err := os.ReadDir(from)
// 	if err != nil {
// 		return err
// 	}
// 	stat, err := os.Stat(to)
// 	if err != nil {
// 		return err
// 	}
// 	if !stat.IsDir() {
// 		return fmt.Errorf("MergeFolder: target %q is not a directory", to)
// 	}

// 	for _, f := range dir {
// 		fromPath := filepath.Join(from, f.Name())
// 		toPath := filepath.Join(to, f.Name())

// 		if _, err := os.Stat(toPath); err != nil {
// 			return fmt.Errorf("MergeFolder: file %q alread exists", toPath)
// 		}

// 		fmt.Println(fromPath, ":", toPath)

// 		break
// 		err = os.Rename(fromPath, toPath)
// 		if err != nil {
// 			return err
// 		}
// 	}
// 	return nil
// }
