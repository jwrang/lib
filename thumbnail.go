package lib

import (
	"bytes"
	"errors"
	"image"
	_ "image/gif"
	"image/jpeg"
	_ "image/png"
	"math"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"golang.org/x/image/draw"
)

func MakeThumbnail(src, dest string) error {
	err := os.MkdirAll(filepath.Dir(dest), Dirperm)
	if err != nil {
		return err
	}

	if IsPicture(src) {
		return makeImgThumbnail(src, dest)
	} else {
		return makeVideoThumbnail(src, dest)
	}
}

func IsPicture(path string) bool {
	switch filepath.Ext(path) {
	case ".jpeg", ".png", ".jpg", ".gif":
		return true
	default:
		return false
	}
}

func makeImgThumbnail(src, dest string) error {
	f, err := os.Open(src)
	if err != nil {
		return err
	}
	defer f.Close()

	img, _, err := image.Decode(f)
	if err != nil {
		return err
	}

	bounds := img.Bounds()
	tw, th := getThumbnailSize(bounds.Dx(), bounds.Dy())

	thumbnail := image.NewRGBA(image.Rect(0, 0, tw, th))
	draw.BiLinear.Scale(thumbnail, thumbnail.Rect, img, img.Bounds(), draw.Over, nil)

	var buf bytes.Buffer
	err = jpeg.Encode(&buf, thumbnail, &jpeg.Options{Quality: 100})
	if err != nil {
		return err
	}

	err = os.WriteFile(dest, buf.Bytes(), Fileperm)
	return err
}

func getThumbnailSize(x, y int) (int, int) {
	const max = 256.0
	xr := float64(x) / max
	yr := float64(y) / max
	r := math.Max(xr, yr)
	return int(math.Floor(float64(x) / r)), int(math.Floor(float64(y) / r))
}

func makeVideoThumbnail(src, dest string) error {
	durStr, err := exec.Command("ffprobe",
		"-i", src,
		"-show_entries", "format=duration",
		"-v", "quiet",
		"-of", "csv=p=0",
	).CombinedOutput()

	if err != nil {
		return errors.New(string(durStr))
	}

	dur, err := strconv.ParseFloat(strings.TrimSpace(string(durStr)), 64)
	if err != nil {
		return err
	}

	good := time.Duration(dur*0.15) * time.Second
	goodSec := int(good.Seconds())

	output, err := exec.Command("ffmpeg",
		"-i", src,
		"-y", // override existing thumbnail
		"-ss", strconv.Itoa(goodSec),
		"-vf", "scale=256:256:force_original_aspect_ratio=decrease", // scale image to max 256x256
		"-frames:v", "1", // get 1 frame
		dest,
	).CombinedOutput()

	if err != nil {
		return errors.New(string(output))
	}
	return nil
}
